<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="asset/css/style.css"> 
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>      
  <script type="text/javascript" src="asset/js/Chart.js"></script>
  <script src="asset/js/jquery-3.5.1.min.js"></script>
  <title>prevent covid-19</title>
</head>
<body>
  <!--nav head-->
  <div>
    <nav class="navbar navbar-expand-lg navbar-light" id="nav-head">
      <span class="navbar-brand mb-0 h1 font1 text-light" style=" font-size: 37px" >data global covid-19</span> 
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto" >
          <li class="nav-item active">
            <a class="nav-link text-light font1" style="font-size: 17px;" href="index.php"> Dashboard <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link text-light font1" style="font-size: 17px;" href="about_covid_19.php"> About Covid-19 <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link font1 disabled" style="font-size: 17px; color:#DBDBDB;" href="prevent_covid.php"> How To Prevent The Spread Of Covid-19 <span class="sr-only">(current)</span></a>
          </li>
          
        </ul>
        <ul class="navbar-nav mr-auto" style="margin-left: 35.9%;" >
          <li class="nav-item active">
            <a class="nav-link text-light"  href="https://github.com/SGWN"><i style="font-size: 26px;" class="fab fa-github"></i><span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </nav>
    </div>
    <!--body content-->
    <div class="container">
      <div class="row">
        <div class="col-12" id="body-content" style="background-color:#FFDFFD;">
          <h2 class="font1 mt-3" style="text-align: center; font-size:40px;" > How can you prevent the spread of COVID-19?</h2>
          <ul style="margin-top: 80px;">
            <li>Wash your hands regularly with soap and water, or clean them with alcohol-based hand rub.</li>
            <li>Maintain at least 1 metre distance between you and people coughing or sneezing.</li>
            <li>Avoid touching your face.</li>
            <li>Cover your mouth and nose when coughing or sneezing.</li>
            <li>Stay home if you feel unwell.</li>
            <li>Refrain from smoking and other activities that weaken the lungs.</li>
            <li>Practice physical distancing by avoiding unnecessary travel and staying away from large groups of people.</li>
          </ul>
          <p style="margin-top: 242px;"></p>
        </div>
      </div>
    </div>
    
    
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    
  </body>
  </html>
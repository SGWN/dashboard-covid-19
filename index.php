<?php


$curl = curl_init();
curl_setopt($curl, CURLOPT_URL,'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, true);

$global =$result['Global'];
$temp_NewConfirmed = (string)$global["NewConfirmed"];
$temp_TotalConfirmed = (string)$global["TotalConfirmed"];
$temp_NewDeath = (string)$global["NewDeaths"];
$temp_TotalDeath = (string)$global["TotalDeaths"];
$temp_NewRecovered = (string)$global["NewRecovered"];
$temp_TotalRecovered = (string)$global["TotalRecovered"];
$countries = $result['Countries'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="asset/css/style.css"> 
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>      
  <script type="text/javascript" src="asset/js/Chart.js"></script>
  <script src="asset/js/jquery-3.5.1.min.js"></script>
  <title>Dashboard</title>
</head>
<body>
  
  <!--nav heading -->
  <div>
    <nav class="navbar navbar-expand-lg navbar-light" id="nav-head">
      <span class="navbar-brand mb-0 h1 font1 text-light" style=" font-size: 37px" >data global covid-19</span> 
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto" >
          <li class="nav-item active">
            <a class="nav-link font1 disabled" style="font-size: 17px; color: #DBDBDB; " href="index.php"> Dashboard <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link text-light font1" style="font-size: 17px;" href="about_covid_19.php"> About Covid-19 <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link text-light font1" style="font-size: 17px;" href="prevent_covid.php"> How To Prevent The Spread Of Covid-19 <span class="sr-only">(current)</span></a>
          </li>
          
        </ul>
        <ul class="navbar-nav mr-auto" style="margin-left: 44%;" >
          <li class="nav-item active">
            <a class="nav-link text-light"  href="https://github.com/SGWN"><i style="font-size: 26px;" class="fab fa-github"></i><span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </nav>
    </div>
    
    <!-- body cards -->
    <div class="container-xl">
      <div class="row">
        <div class="col-12 bg-light">
          
          <div class="card-group mt-2" style="width: 100%;" >
            <div class="card" >
              <div class="card-body bg-danger">
                <h5 class="card-title" style="font-size:40px;" ><img src="https://img.icons8.com/windows/32/000000/ambulance.png"/></h5>
                <h5 class="card-title font1 text-light" style="font-size: 20px;" >New Confirmed</h5>
                <p class="card-text text-light font-weight-bold " style="font-size:19px" ><?= number_format($temp_NewConfirmed); ?></p>
              </div>
            </div>
            
            <div class="card">
              <div class="card-body bg-dark">
                <h5 class="card-title" style="font-size:40px;" ><img src="https://img.icons8.com/windows/43/000000/cemetery.png"/></h5>
                <h5 class="card-title font1 text-light " style="font-size: 20px;" >New Death</h5>
                <p class="card-text text-light font-weight-bold  " style="font-size:19px" ><?= number_format($temp_NewDeath); ?></p>
              </div>
            </div>
            
            <div class="card">
              <div class="card-body bg-primary">
                <h5 class="card-title" style="font-size:40px;" ><img src="https://img.icons8.com/metro/26/000000/hospital.png"/></h5>
                <h5 class="card-title font1 text-light" style="font-size: 20px;" >New Recovered</h5>
                <p class="card-text text-light font-weight-bold " style="font-size:19px" ><?= number_format($temp_NewRecovered); ?></p>
              </div>
            </div>
            
            <div class="card">
              <div class="card-body bg-danger">
                <h5 class="card-title" style="font-size:40px;" ><img src="https://img.icons8.com/windows/32/000000/ambulance.png"/></h5>
                <h5 class="card-title font1 text-light" style="font-size: 20px;" >Total Confirmed</h5>
                <p class="card-text text-light font-weight-bold " style="font-size:19px" ><?= number_format($temp_TotalConfirmed); ?></p>
              </div>
            </div>
            
            <div class="card">
              <div class="card-body bg-dark">
                <h5 class="card-title" style="font-size:40px;" ><img src="https://img.icons8.com/windows/43/000000/cemetery.png"/></h5>
                <h5 class="card-title font1 text-light" style="font-size: 20px;" >Total Death</h5>
                <p class="card-text text-light font-weight-bold" style="font-size:19px" ><?= number_format($temp_TotalDeath); ?></p>
              </div>
            </div>
            
            <div class="card">
              <div class="card-body bg-primary">
                <h5 class="card-title" style="font-size:40px;" ><img src="https://img.icons8.com/metro/26/000000/hospital.png"/></h5>
                <h5 class="card-title font1 text-light" style="font-size: 20px;" >Total Recovered</h5>
                <p class="card-text text-light font-weight-bold " style="font-size:19px" ><?= number_format($temp_TotalRecovered); ?></p>
              </div>
            </div>
            
          </div>
          
          
        </div>                 
      </div>
    </div>
    <!-- header chart -->
    <h2 class="font1" style="text-align: center; font-size:40px " > countries affected by corona</h2>
    <div class="container-xl">
      <div class="row">
        <!-- chart -->
        <div class="col-8 mt-2">
          <div class="overflow">
            
            
            <div  >
              <canvas id="myChart" width="200px" height="950px"></canvas>
            </div>
            
            
            <script>
              var ctx = document.getElementById("myChart").getContext('2d');
              
              var data=$.ajax({
                url: 'https://api.covid19api.com/summary',
                Cache:false   
              })
              
              .done(function (html){
                function get_all_countries(html){
                  var temp_arr=[];
                  html.Countries.forEach(function (el){
                    temp_arr.push(el.Country);
                  })
                  return temp_arr;
                }
                
                function get_total(html){
                  var temp_total=[];
                  html.Countries.forEach(function (co){
                    temp_total.push(co.TotalConfirmed);
                  })
                  return temp_total;
                }
                
                var colors=[];
                function color_random(){
                  var r = Math.floor(Math.random() * 255);
                  var g = Math.floor(Math.random() * 255);
                  var b = Math.floor(Math.random() * 255);
                  return "rgb(" + r + "," + g + "," + b + ")";
                }
                for (var i in html.Countries) {
                  colors.push(color_random());
                }
                
                var myChart = new Chart(ctx, {
                  type: 'horizontalBar',
                  data: {
                    labels: get_all_countries(html),
                    datasets: [{
                      
                      data: get_total(html),
                      backgroundColor: colors,
                      borderColor: colors,
                      borderWidth: 0.1
                    }]
                  },
                  options: {
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: 'black'
                      },
                      position: 'left',
                      display: false,
                    }
                  }
                  
                })
              });
            </script>
          </div>  
        </div>
        <div class="col-4 mt-2">
          <div id="carouselExampleInterval" class="carousel slide "  style="border-style: solid; border-color:black;" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active" data-interval="10000">
                <div class="card  font1">
                  <div class="card-header bg-dark text-light" style="font-size: 28px;">
                    countries
                  </div>
                  <div class="card-body">
                    <h5 class="card-title" style="margin-top: 20px;" > Confirmed :</h5>
                    <h5 class="card-text mt-3"> Death : </h5>
                    <h5 class="card-text mt-3"> Recovered : </h5>
                    <h5 class="card-text" style="margin-top: 66px;" ></h5>
                    
                  </div>
                </div>
              </div>
              <?php foreach($countries as $key): ?>
              <div class="carousel-item" data-interval="2000">
                <div class="card font1">
                  <div class="card-header bg-dark text-light" style="font-size: 28px;">
                    <?php echo $key['Country']; ?>
                  </div>
                  <div class="card-body">
                    
                    <h5 class="card-title" style="margin-top: 20px;" > Confirmed : <?php echo number_format($key['TotalConfirmed']);?></h5>
                    <h5 class="card-text mt-3"  > Death : <?php echo number_format($key['TotalDeaths']);?></h5>
                    <h5 class="card-text mt-3"> Recovered : <?php echo number_format($key['TotalRecovered']);?></h5>
                    <h5 class="card-text" style="margin-top: 66px;" ></h5>
                    
                  </div>
                </div>
              </div>
              <?php endforeach;?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
    
    
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    
  </body>
  </html>
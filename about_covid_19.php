<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="asset/css/style.css"> 
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>      
  <script type="text/javascript" src="asset/js/Chart.js"></script>
  <script src="asset/js/jquery-3.5.1.min.js"></script>
  <title>About Covid-19</title>
</head>
<body>
  
  <!-- navbar header -->
  <div>
    <nav class="navbar navbar-expand-lg navbar-light" id="nav-head">
      <span class="navbar-brand mb-0 h1 font1 text-light" style=" font-size: 37px" >data global covid-19</span> 
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto" >
          <li class="nav-item active">
            <a class="nav-link text-light font1" style="font-size: 17px;" href="index.php"> Dashboard <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link font1 disabled" style="font-size: 17px; color: #DBDBDB;" href="about_covid_19"> About Covid-19 <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link text-light font1" style="font-size: 17px;" href="prevent_covid.php"> How To Prevent The Spread Of Covid-19 <span class="sr-only">(current)</span></a>
          </li>
          
        </ul>
        <ul class="navbar-nav mr-auto" style="margin-left: 35%;" >
          <li class="nav-item active">
            <a class="nav-link text-light"  href="https://github.com/SGWN"><i style="font-size: 26px;" class="fab fa-github"></i><span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </nav>
    </div>
    
    <!-- body content -->
    <div class="container">
      <div class="row">
        <div class="col-12" id="body-content" style="background-color:#FFDFFD;" >
          <h2 class="font1 mt-3" style="text-align: center; font-size:40px;" > COVID-19 </h2>
          <p>Coronavirus disease 2019 ( COVID-19 ) is a contagious
            respiratory and vascular disease caused by severe acute respiratory syndrome
            coronavirus 2 (SARS-CoV-2). First identified in Wuhan , China, it has caused
            an ongoing pandemic.
          </p>
          <h2 class="font1 mt-4" style="text-align: center; font-size:40px;" > Signs And Symptoms </h2>
          <p>Symptoms of COVID-19 are variable, but usually include fever and a cough. People with the same
            infection may have different symptoms, and their symptoms may change over time. For example, one
            person may have a high fever, a cough, and fatigue, and another person may have a low fever at the
            start of the disease and develop difficulty breathing a week later. However, in people without 
            prior ears, nose, and throat ( ENT ) disorders, loss of taste combined with loss of smell is 
            associated with COVID-19 with a specificity of 95%.</p>
            <p class="mt-4">
              Some symptoms of COVID-19 can be relatively non-specific, the two most common symptoms
              are fever (88 percent) and dry cough (68 percent). Among those who develop symptoms, 
              approximately one in five may become more seriously ill and have difficulty breathing.
              Emergency symptoms include difficulty breathing, persistent chest pain or pressure,
              sudden confusion, difficulty waking, and bluish face or lips, immediate medical attention
              is advised if these symptoms are present. Further development of the disease can lead to
              complications including pneumonia, acute respiratory distress syndrome, sepsis, septic
              shock, and kidney failure.
            </p>
            <p class="mt-4">
              As is common with infections, there is a delay,
              known as the incubation period , between the moment a person first becomes infected and the
              appearance of the first symptoms. The median incubation period for COVID-19 is four to five days.
              Most symptomatic people experience symptoms within two to seven days after exposure, and almost
              all symptomatic people will experience one or more symptoms before day twelve.
            </p>
          </div>
        </div>
      </div>
      
      
      
      <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      
      <!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
      
    </body>
    </html>